from FibUtility import *
from newPlotter import *
from TradingData import *

import pandas as pd
import numpy as np

if __name__ == '__main__':
    
    from copy import deepcopy
    
    coin_name = 'ETH'
    interval = '4h'
    start_date = '3/1/2021'
    end_date = '2/1/2022'
    
    Data = TradingData(coin_name, interval, start_date+ ' 00:00:00', end_date+' 23:59:59')
    n = 20
    
    df = deepcopy(Data.df[:n*10+1])

    FibObj = FibRetracement(df, coin_name, n)    
    
    for i in range(Data.df.shape[0]):
        if i <= n*10+1:
            continue 
        p3 = Data.df.iloc[i]    
        p3 = pd.DataFrame(p3)
        p3 = p3.T
        
        FibObj.addData(p3)
    
    FibObj.UpdateExtremas()
    FibObj.FilterFibLines()
    FibObj.PlotFibPairs(1, start_date, end_date)