from TradingData import *
from newPlotter import *

import numpy as np
import pandas as pd
from scipy.signal import argrelextrema

from copy import deepcopy
import math
from heapq import heapify, heappush, heappop

import time

class FibPairs:
    
    def __init__(self, p1, p2, toplot:int=0):
        
        self.scoreY = abs(p1['max'] - p2['min'])
        self.scoreX = abs(p1['candleID'] - p2['candleID'])
        self.toplot = toplot
        self.p1 = p1
        self.p2 = p2
        
        if p1['candleID'] < p2['candleID']:
            self.trend = 'down'
            self.x1 = p1['candleID']
            self.x2 = p2['candleID']
            self.y1 = p1['max']
            self.y2 = p2['min']
        else:
            self.trend = 'up'
            self.x2 = p1['candleID']
            self.x1 = p2['candleID']
            self.y2 = p1['max']
            self.y1 = p2['min']
            
        self.ex2 = self.x2
        self.ex1 = self.x1
        self.ey2 = self.y2
        self.ey1 = self.y1
        
    def _eq__(self, other):
        return (self.x1 == other.x1 and self.x2 == other.x2)

    def __repr__(self):
        return (str(self.x1) + ' ' + str(self.x2) + ' ' + str(self.y1) + ' ' + str(self.y2) + ' ' + str(self.scoreY) +  '\n' + 
                str(self.ex1) + ' ' + str(self.ex2) + ' ' + str(self.ey1) + ' ' + str(self.ey2) )
        
    
class FibRetracement:
    
    def __init__(self, df, n:int = 25):
        
        self.df = df
        self.n = n
        
        self.df['minval'] = self.df[['open', 'close']].min(axis=1) 
        self.df['maxval'] = self.df[['open', 'close']].max(axis=1)
        
        self.df['min'] = self.df.iloc[argrelextrema(self.df.low.values, np.less_equal, order = (self.n//3))[0]]['low']
        self.df['max'] = self.df.iloc[argrelextrema(self.df.high.values, np.greater_equal, order = (self.n//3))[0]]['high']
        
        self.new_count = 0                                  # keeps the count of new candlestick added 
        self.price_range = self.df['max'].max() - self.df['min'].min()
        
        self.window_size = self.n * 10
        
        self.fibLevelpts = [0,0.236, 0.382, 0.5 , 0.618, 0.786,1, 1.618]
        
        self.fibpairs = []
        # self.heapFib = []
        self.reserve_set = []
        # heapify(self.heapFib)
        self.maxY = 0
        self.temp_df = pd.DataFrame(columns=self.df.columns)
        self.FindNewFibLevel()
    
    def UpdateExtremas(self, flag:int = 0):
        self.temp_df['min'] = self.temp_df.iloc[argrelextrema(
            self.temp_df.low.values, np.less_equal, order = (self.n//3))[0]]['low']
        self.temp_df['max'] = self.temp_df.iloc[argrelextrema(
            self.temp_df.high.values, np.greater_equal, order = (self.n//3))[0]]['high']

        self.df = pd.concat([self.df, self.temp_df])
        self.price_range = self.df['max'].max() - self.df['min'].min()
        
        self.temp_df = pd.DataFrame(columns=self.df.columns)
    
    
    def addData(self, newData):
        
        self.new_count = self.new_count+1
        
        newData['minval'] = newData[['open', 'close']].min(axis = 1)
        newData['maxval'] = newData[['open', 'close']].max(axis = 1)
        
        self.temp_df = pd.concat([self.temp_df, newData])
         
        if (self.new_count//self.n)*self.n == self.new_count :    
            self.UpdateExtremas()
            
        kwind = self.n*2
        if (self.new_count//kwind)*kwind == self.new_count:
            self.FindNewFibLevel()
            
    # p1 is dfMax point and p2 is dfMin point
    
    def CheckFibLevel(self, p1, p2, flag:int=0):
        
        if flag == 0:
        
            for k in self.fibpairs:
                d1 = abs(p1['max'] - k.p1['max'])
                d2 = abs(p2['min'] - k.p2['min'])
                
                # print(d1, d2, p1['candleID'], p2['candleID'], p1['max'] ,k[0]['max'], p2['min'],k[1]['min'])
                
                if d1 < self.price_range*(0.05) and d2 < self.price_range*(0.05):
                    return -1    # present
        
        tempfib = FibPairs(p1, p2)
        
        if tempfib in self.reserve_set:
            return 0
        
        d = abs(p1['max'] - p2['min']) * abs(p1['candleID'] - p2['candleID'])
        
        if tempfib.scoreY < self.price_range*0.3:
            return 0
        
        if tempfib.scoreX < self.n*2:
            return 0
        
        # if d > self.price_range*4:
        if p1['candleID'] > p2['candleID']:
            return 1  # main is uptrend
        else:
            return 2  # downtrend
            
        # else:
        #     return 0  # not good to form
                
          
    def FindNewFibLevel(self):
        
        print("new iteration")
        dfMax = self.df[self.df['max'].notnull()]
        dfMin = self.df[self.df['min'].notnull()]
        
        for i1 in range(dfMax.shape[0]):
            for i2 in range(dfMin.shape[0]):
                
                p1 = dfMax.iloc[i1]
                p2 = dfMin.iloc[i2]
                
                if abs(p1['candleID'] - p2['candleID']) < (self.n)//2:
                    continue
                
                if (p1['max'] < p2['min']):
                    continue
                
                c = self.CheckFibLevel(p1, p2, 1)
                
                if c <= 0:
                    continue
                
                if i1 > 0 and i1 < dfMax.shape[0]-1:
                    if p1['max'] < dfMax.iloc[i1-1]['max'] and p1['max'] < dfMax.iloc[i1+1]['max']:
                        continue
                
                if i2 > 0 and i2 < dfMin.shape[0]-1:
                    if p2['min'] > dfMin.iloc[i2-1]['min'] and p2['min'] > dfMin.iloc[i2+1]['min']:
                        continue
                
                score = abs(p1['max'] - p2['min'])
                tempfib = FibPairs(p1, p2)
                
                if c >= 1:
                    self.fibpairs.append(tempfib)
                    self.maxY = max(score, self.maxY)
        
        self.FilterFibLines()       
        # self.PlotFibPairs()

    def coverFibLines(self, t1, t2):
        # here first one is overlapped to second that is new line t3 is t1 V t2 
        # diagram
        t1.ex1 = min(t1.x1, t2.x1)
        t1.ex2 = max(t1.x2, t2.x2)
        
        lst = [t1.y1, t2.y1, t1.y2, t2.y2]
        mx = max(lst)
        mn = min(lst)
        
        if t1.trend == 'up':
            t1.ey1 = mn
            t1.ey2 = mx
        else:
            t1.ey1 = mx
            t1.ey2 = mn
            
        return t1

    def FilterFibLines(self):
        rm_fib = []
        
        self.fibpairs.sort(key = lambda x: x.scoreY, reverse=True)
        s = self.fibpairs[0].scoreY
        
        for t1 in self.fibpairs:
            if t1 in rm_fib:
                continue
            
            for t2 in self.fibpairs:
                if t2 in rm_fib:
                    continue
            
                if t1 == t2:
                    continue
                            
                d1 = abs(t1.p1['max'] - t2.p1['max'])
                d2 = abs(t1.p2['min'] - t2.p2['min'])
        #         # print(d1, d2)
                h1 = t1.scoreY
                h2 = t2.scoreY
        #         # print(d1, d2)
                lst = [abs(t1.x1 - t2.x1), abs(t1.x1 - t2.x2), abs(t1.x2 - t2.x1), abs(t1.x2 - t2.x2)]
                diff = min(lst)
                
                l1 = abs(t1.x2 - t1.x1) 
                l2 = abs(t2.x2 - t2.x1)
                l = max(abs(t1.x1 - t2.x2), abs(t1.x2 - t2.x1))
                dff = l1 + l2 - l
                
                if dff > 0:      
                    if d1+d2 < self.price_range*(0.1) and abs(h1 - h2)/max(h1,h2) <= (0.1)*max(h1,h2):
                        if h1 < h2:
                            rm_fib.append(t1)
                            t2 = self.coverFibLines(t2, t1)
                        else:
                            rm_fib.append(t2)
                            t1 = self.coverFibLines(t1, t2)
                    
                          
        for t in rm_fib:
            self.fibpairs.remove(t)
            self.reserve_set.append(t)
            
        # # removing the fib lines which are very smaller than maxheight
        rm = []
        for t1 in self.fibpairs:
            
            if t1.scoreY < 0.6 * s:
                rm.append(t1)
                continue
            
            elif t1.scoreY < 0.92*s and t1.scoreY >= 0.8*s:
                rm.append(t1)
                continue
            
        for t in rm:
            self.fibpairs.remove(t)
            self.reserve_set.append(t)
        
                
    
    def PlotFibPairs(self):
        # self.FilterFibLines()
        
        colors = ["black","r","g","b","cyan","magenta","yellow", "o"]
        
        c = 1
        for i in self.fibpairs:
            # c = c+1
            # if c%2 == 0:
            #     continue
            # if i.toplot == 0:
            i.toplot = 1
            fig = PlotAddCandleSticks(self.df)
            p1 = i.p1
            p2 = i.p2
            
            minval = p2['min']
            minCid = p2['candleID']
            
            maxval = p1['max']
            maxCid = p1['candleID']
            
            if i.trend == 'up':
                minval = i.ey1
                minCid = i.ex1
                maxval = i.ey2
                maxCid = i.ex2
            else:
                minval = i.ey2
                minCid = i.ex2
                maxval = i.ey1
                maxCid = i.ex1
            
            diff = maxval - minval
            
            print(diff, p2['min'], p2['candleID'], p1['max'], p1['candleID'])
            
            for j in range(len(self.fibLevelpts)):
                intrmx = minval + (self.fibLevelpts[j] * diff)
                PlotFibLine(fig, self.fibLevelpts[j], intrmx, colors[j], maxCid, minCid)
            plot(fig, 'FibLevel' + '.html')
    
    def checkfunc(self):
        
        for t1 in self.fibpairs:    
            
            for t2 in self.fibpairs:
                
                if t1.scoreY <= t2.scoreY:
                    continue
                    
                l1 = abs(t1.x2 - t1.x1) 
                l2 = abs(t2.x2 - t2.x1)
                l = max(abs(t1.x1 - t2.x2), abs(t1.x2 - t2.x1))
                dff = l1 + l2 - l
                
                d1 = abs(t1.p1['max'] - t2.p1['max'])
                d2 = abs(t1.p2['min'] - t2.p2['min'])
                print(t1.scoreY, t2.scoreY)
                print(d1, d2, self.price_range*0.1, dff)

if __name__ == '__main__':
    
    from copy import deepcopy
    
    Data = TradingData('BNB', '4h', '12/1/2021 00:00:00', '2/1/2022 23:59:59')
    n = 20
    df = deepcopy(Data.df[:n*10+1])

    FibObj = FibRetracement(df, n)    
    
    for i in range(Data.df.shape[0]):
        if i <= n*10+1:
            continue 
        p3 = Data.df.iloc[i]    
        p3 = pd.DataFrame(p3)
        p3 = p3.T
        
        FibObj.addData(p3)
        
    
    FibObj.UpdateExtremas(1)
    FibObj.FilterFibLines()
    FibObj.PlotFibPairs()
    
    FibObj.checkfunc()   
    


